import re
import sys

pattern = r'([a-zA-Z#]+)([0-9]+)(\.*)([a-zA-Z_\-0-9]+)'

notes = {'a':0, 'a#':1, 'b':2, 'c':3, 'c#':4, 'd':5, 'd#':6, 'e':7, 'f':8, 'f#':9, 'g':10, 'g#':11}

def m2f(m):
    return 440.0 * 2 ** ((m - 69) / 12.0)

def say(note, word, duration=200):
    global words
    m = m2f(note)
    print "~"
    for syllable in words[word]:
        print '%s {D %f; P %f:0 %f:100}' % (syllable[0], syllable[1] * duration, m, m)

base_note = 69
rate = 500

def set_tempo(tempo):
    global rate
    rate = 240000.0 / tempo


data = ''
for arg in sys.argv[1:]:
    data += file(arg).read() + '\n\n'

words = {}

tokens = data.split()

def token_get():
    global tokens
    if len(tokens):
        token = tokens[0]
        tokens = tokens[1:]
    else:
        return None
    return token

def parse_note(token):
    global pattern, base_note, rate, notes
    match = re.match(pattern, token)

    note = notes[match.group(1)] + base_note
    division = float(match.group(2))
    base_div = division
    r = rate / division
    dots = len(match.group(3))
    for i in range(dots):
        division *= 2.0
        r += rate / division
    word = match.group(4)
    say(note, word, r)

print "[[inpt TUNE]]"

token = token_get()
while token:
    if token == 'tempo':
        set_tempo(float(token_get()))
    elif token == 'oct+':
        base_note += 12
    elif token == 'oct-':
        base_note -= 12
    elif token == 'word':
        sub_tokens = []
        sub_t = token_get()
        while sub_t != 'end':
            sub_tokens.append(sub_t)
            sub_t = token_get()
        name = sub_tokens[0]
        sub_tokens = sub_tokens[1:]
        words[name] = []
        while len(sub_tokens):
            words[name].append((sub_tokens[0], float(sub_tokens[1])))
            sub_tokens = sub_tokens[2:]
    else:
        parse_note(token)
    token = token_get()

print "[[inpt TEXT]]"

